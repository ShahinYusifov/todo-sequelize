const express = require("express");
const app = express();
const userRouter = require("./routes/userRoute");
const taskRouter = require("./routes/taskRoute");
const registerRouter = require("./routes/loginRegister");
const sequelize = require("./dbConnection");
const dotenv = require('dotenv').config();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/users", userRouter);
app.use("/tasks", taskRouter);
app.use(registerRouter);

sequelize.sync().then(()=>{
  app.listen(process.env.PORT, () => {
    console.log(`Server is running at ${process.env.PORT}`);
  });
}).catch(err=>console.log(err))

