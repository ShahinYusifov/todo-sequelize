const { Sequelize, DataTypes } = require("sequelize");
const sequelize = require("../dbConnection");


const Task = sequelize.define(
  "Tasks",
  {
    title: {
      type: DataTypes.STRING,
      required: true,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      required: true,
      allowNull: false,
    },
    is_done: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    is_deleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    }
  },
  {
    timestamps:false
  }
);

Task.sync({alter: true}).then(() => {
  // console.log("Users table created successfully!");
})
.catch((error) => {
  console.error("Unable to create table : ", error);
});
// sequelize
//   .sync()
//   .then(() => {
//     console.log("Users table created successfully!");
//   })
//   .catch((error) => {
//     console.error("Unable to create table : ", error);
//   });

module.exports = Task;
