const { Sequelize, DataTypes } = require("sequelize");
const sequelize = require("../dbConnection");

const User = sequelize.define(
  "Users",
  {
    name: {
      type: DataTypes.STRING,
      required: true,
      allowNull: false,
    },
    username: {
      type: DataTypes.STRING,
      required: [true, "Please enter a username"],
      allowNull: false,
    },
    age: {
      type: DataTypes.INTEGER,
      required: true,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      required: true,
      allowNull: false,
    }
  },
  {
    timestamps: false
  }
);


sequelize
  .sync({alter:true})
  .then(() => {
    // console.log("Users table created successfully!");
  })
  .catch((error) => {
    console.error("Unable to create table : ", error);
  });

module.exports = User;
