const express = require("express");
const router = express.Router();
const sequelize = require("../dbConnection");
const User = require("../models/userModel");

router.get("/register", (req, res) => {
  res.send("This is register page");
});

router.post("/register", (req, res) => {
  sequelize.sync().then(() => {
    User.create({
      name: req.body.name,
      username: req.body.username,
      age: req.body.age,
      password: req.body.password,
    })
      .then((data) => {
        res.status(201).json({
          success: true,
          message: data.toJSON(),
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          message: err,
        });
      });
  });
});

router.get("/login", () => {
  res.send("This is login page");
});

router.post("/login", async (req, res) => {
  let username = req.body.username;
  let password = req.body.password;
  sequelize
    .sync()
    .then( async () => {
      let user = await User.findOne({ where: { username,password } });
      if (!user) {
        res.send("Username or password is wrong!");
      }
    })
    .then(() => {
      if (password) {
        res.send("Logged in successfully");
      }
    })
    .catch((err) => {
      console.log(err);
    });
});

module.exports = router;
