const express = require("express");
const sequelize = require("../dbConnection");
const router = express.Router();
const Task = require("../models/taskModel");

router.delete("/delete-task/:id", (req,res)=>{
  sequelize.sync().then(()=>{
    Task.destroy({where: {id: req.params.id}})
  })
})

router.put("/update-task/:id", (req,res)=>{
  sequelize.sync().then(()=>{
    Task.update({
      title: req.body.title,
      description: req.body.description
    },{where: {id: req.params.id}})
  })
})

router.post("/add-task", (req, res) => {
  sequelize.sync().then(() => {
    Task.create({
      title: req.body.title,
      description: req.body.description,
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  });
});

router.put("/completed/:id", (req, res) => {
  sequelize.sync().then(() => {
    Task.update(
      { is_completed: true },
      {
        where: { id: req.params.id },
      }
    )
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  });
});

router.put("/non-completed/:id", (req, res) => {
  sequelize.sync().then(() => {
    Task.update(
      { is_completed: false },
      {
        where: { id: req.params.id },
      }
    )
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  });
});

router.get("/non-completed", (req, res) => {
  sequelize.sync().then(() => {
    Task.findAll({ where: { is_completed: false } })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  });
});

router.get("/completed", (req, res) => {
  sequelize.sync().then(() => {
    Task.findAll({ where: { is_completed: true } })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  });
});

router.get("/tasks", (req, res) => {
  sequelize.sync().then(() => {
    Task.findAll({where: {is_deleted: false}})
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  });
});

module.exports = router;
