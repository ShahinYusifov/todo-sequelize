const express = require("express");
const sequelize = require("../dbConnection");
const router = express.Router();
const User = require("../models/userModel");

router.delete("/delete-user/:id", (req, res)=>{
  sequelize.sync().then(()=>{
    User.destroy({where: {id: req.params.id}})
  }).then((res) => {
    res.status(200).json({
      success: true,
      message: res,
    });
  })
  .catch((err) => {
    res.status(401).json({
      success: false,
      message: err.message,
    });
  });
})

router.put("/update-user/:id", (req, res) => {
  sequelize.sync().then(() => {
    User.update(
      {
        name: req.body.name,
        surname: req.body.surname,
        age: req.body.age,
      },
      {
        where: {
          id: req.params.id,
        },
      }
    )
      .then((res) => {
        res.status(200).json({
          success: true,
          message: res,
        });
      })
      .catch((err) => {
        res.status(401).json({
          success: false,
          message: err,
        });
      });
  });
});

router.get("/users-list", (req, res) => {
  sequelize.sync().then(() => {
    User.findAll()
      .then((data) => {
        res.status(200).json({
          success: true,
          message: data.toJSON(),
        });
      })
      .catch((err) => {
        res.status(401).json({
          success: false,
          message: err,
        });
      });
  });
});

module.exports = router;
